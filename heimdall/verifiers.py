import re
import functools


@functools.lru_cache(maxsize=None)
def compile_pattern(pattern: str) -> re.Pattern:
    return re.compile(pattern)


def assert_pattern(variable, pattern: str):
    pattern = compile_pattern(pattern)
    if not pattern.match(str(variable)):
        raise ValueError(f"{variable} does not match {pattern}")


def assert_type(variable, expected_type: type):
    if not isinstance(variable, expected_type):
        raise TypeError(
            f"{variable} is not type {expected_type.__name__}")
