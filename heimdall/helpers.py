import functools

from types import FunctionType


class Base(object):
    def __init__(self, *args, **kwargs):
        """
        __init__ receives the arguments given to the decorator
        """
        self.args = args
        self.kwargs = kwargs

    def __call__(self, func: FunctionType):
        @functools.wraps(func)
        def _decorate(*args, **kwargs):
            return self.handle(func, args, kwargs)
        return _decorate

    def handle(self, func: FunctionType, args: tuple, kwargs: dict):
        return func(*args, **kwargs)
