from types import FunctionType
from typing import get_type_hints, Union

from heimdall import verifiers, helpers


# Issue with this approach, means having a reserved keyword (eg. 'mode')
# to select a mode.
# Any parameter 'mode' will get caught and reinterpreted :(
MODES = {
    'default': verifiers.assert_pattern,
}


class Verify(helpers.Base):
    """
    Verify if the passed function arguments match provided regex patterns.

    By default this also checks if the correct types are provided.
    This can be dissabled by setting verify_types to False
    """
    verify_types = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rules = self.kwargs

    def _get_function_arguments(
            self, func: FunctionType, values: tuple) -> dict:
        """
        map the called function's postitional arguments to their positional
        values.
        (foo, bar) ("some", "value") -> { "foo": "some", "bar": "value" }
        """
        varnames = func.__code__.co_varnames

        # This expects a very strict ordering!!
        result = {}
        for i in range(0, len(values)):
            result[varnames[i]] = values[i]

        return result

    def handle(self, func: FunctionType, args: tuple, kwarameters: dict):
        args = self._get_function_arguments(func, args)

        hints = self._get_hints(func)

        variables = {**args, **kwarameters}

        for variable, value in variables.items():
            if variable in self.rules:
                verifiers.assert_pattern(value, self.rules[variable])

            if self.verify_types and variable in hints:
                verifiers.assert_type(value, hints[variable])

        return func(*args, **kwarameters)

    def _get_hints(self, method: FunctionType) -> dict:
        return get_type_hints(method)


class OnlyWhen(helpers.Base):
    """
    Conditionally check whether a function should execute
    """

    def __init__(self, condition: Union[FunctionType, bool]):
        self.condition = condition

    def handle(self, func: FunctionType, args: tuple, kwarameters: dict):
        if isinstance(self.condition, FunctionType):
            if self.condition():
                return func(*args, **kwarameters)

        elif self.condition is True:
            return func(*args, **kwarameters)
