from setuptools import setup, find_packages


setup(
    name="to_be_deicided",
    version="0.1.0",
    description=(
        "A decorator to match the content of arguments against a constraint."),
    license="GPLv3",
    url="https://gitlab.com/bartwillems/to-be-deicided",
    packages=find_packages()
)
