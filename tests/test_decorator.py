import pytest

import heimdall


def test_non_matching_pattern_raises_exception():
    @heimdall.Verify(foo="[a-z]")
    def _temp(foo: str):
        pass

    with pytest.raises(ValueError):
        _temp(foo='KAK69')


def test_non_matching_type_raises_exception():
    @heimdall.Verify(foo="[0-9]")
    def _temp(foo: int):
        pass

    with pytest.raises(TypeError):
        _temp(foo="5")


def test_matching_type():
    @heimdall.Verify(foo="[0-9]")
    def _temp(foo: int):
        pass

    _temp(foo=5)


def test_multiple_parameters():
    @heimdall.Verify(foo="[a-z]", bar="[A-Z]", baz="[0-9]")
    def _temp(foo: str, bar: str, baz: int):
        pass

    _temp(foo="test", bar="TEST", baz=5)


def test_unnamed_parameters():
    @heimdall.Verify(foo="[a-z]", bar="[A-Z]", baz="[0-9]")
    def _temp(foo: str, bar: str, baz: int):
        pass

    _temp("test", "TEST", 5)


def test_mixed_parameters():
    @heimdall.Verify(foo="[a-z]", bar="[A-Z]", baz="[0-9]")
    def _temp(foo: str, bar: str, baz: int):
        pass

    _temp("test", "TEST", baz=5)


def test_parameter_ordering():
    @heimdall.Verify(lower="[a-z]", upper="[A-Z]", number="[0-9]")
    def _temp(lower: str, upper: str, number: int):
        pass

    # Here, number & upper are swapped, this should still work
    _temp("test", number=5, upper="TEST")


def test_env_limiter():
    condition = False

    def should_run() -> bool:
        return condition is True

    @heimdall.OnlyWhen(True)
    def always_execute() -> bool:
        return True

    @heimdall.OnlyWhen(False)
    def never_execute() -> bool:
        return True

    @heimdall.OnlyWhen(should_run)
    def feature_flipper() -> bool:
        return True

    assert always_execute()
    assert never_execute() is None
    assert feature_flipper() is None

    # Override the condition at runtime
    condition = True

    assert feature_flipper() is True
