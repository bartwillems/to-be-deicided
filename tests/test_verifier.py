import pytest

from heimdall import verifiers


def test_matching_pattern_gets_approved():
    rules = {"foo": "[a-z]"}
    parameters = {"foo": "asdfjkl"}

    try:
        verifiers.assert_pattern(parameters["foo"], rules["foo"])
    except BaseException:
        pytest.fail("Pattern did not match content.")


def test_non_matching_pattern_raises_exception():
    rules = {"foo": "[a-z]"}
    parameters = {"foo": "123"}

    with pytest.raises(ValueError):
        verifiers.assert_pattern(parameters["foo"], rules["foo"])


def test_integer_against_regex_succeeds():
    rules = {"foo": "[0-9]"}
    parameters = {"foo": 5}
    try:
        verifiers.assert_pattern(parameters["foo"], rules["foo"])
    except BaseException:
        pytest.fail("Pattern did not match content.")
